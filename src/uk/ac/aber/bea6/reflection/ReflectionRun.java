package uk.ac.aber.bea6.Reflection;
import java.util.Scanner;

/**
 * 
 * @author Ben Ashwell (bea6)
 *
 */
public class ReflectionRun {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		Reflectionclass reflection = new Reflectionclass();
		//take input through command line
		Scanner input = new Scanner(System.in);		
		System.out.println("Would you like to search for the details of one class(one), multiple classes(multiple), one class recursively(one recursive) or multiple classes recursively(multiple recursively)?\n");
		String inputLine = input.nextLine();
		if(inputLine.equals("one"))
		{
			System.out.println("Please enter the class name");
			String className = input.nextLine();
			reflection.getClassStatistics(className);
			reflection.printClassStatistics(className);
		}
		else if(inputLine.equals("multiple"))
		{
			System.out.println("Please enter the file name");
			reflection.getMultipleClassStatistics(input.nextLine());
		}
		else if(inputLine.equals("one recursive"))
		{
			System.out.println("Please enter the class name");
			String className = input.nextLine();
			reflection.recursiveClassStatistics(className);
		}
		else if(inputLine.equals("multiple recursive"))
		{
			System.out.println("Please enter the file name");
			reflection.recursiveMultipleClassStatistics(input.nextLine());
		}
		else
		{
			System.out.println("You have not entered a correct input.");
			System.exit(0);
		}
	}
}
