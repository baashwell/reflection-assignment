package uk.ac.aber.bea6.Reflection;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.*;
import java.util.Hashtable;

/**
 * 
 * @author Ben Ashwell (bea6)
 * 
 */
public class Reflectionclass {

	/**
	 * A data structure to hold the class names as a key that links to a class
	 * holding all the statistics.
	 */
	private Hashtable<String, ClassStatistics> dataStructure = new Hashtable<String, ClassStatistics>();

	/**
	 * Method to run when reading in a file, to read in each line, get
	 * statistics for each class and store in the hash table.
	 * 
	 * @param fileName
	 *            the file location to read in.
	 */
	public void getMultipleClassStatistics(String fileName) {
		String readLine;
		BufferedReader reader;
		ClassStatistics statisticsToAdd;

		try {
			reader = new BufferedReader(new FileReader(fileName));

			// while there is still more to read, get statics for class and put
			// it into the hashtable. The print out the statistics.
			while ((readLine = reader.readLine()) != null) {
				statisticsToAdd = getClassStatistics(readLine);
				dataStructure.put(readLine, statisticsToAdd);
				printClassStatistics(readLine);
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}

	}

	/**
	 * Method to print the statistics of a class, passing the class name and
	 * statistics of the class.
	 * 
	 * @param className
	 *            The name of the class.
	 */
	public void printClassStatistics(String className) {
		ClassStatistics statistics = dataStructure.get(className);

		if (statistics != null) {
			System.out.println("Reaserching Class " + className);
			System.out.println("The Total Methods in this class are "
					+ statistics.getTotalMethods());
			System.out.println("The Total Public Methods in this class are "
					+ statistics.getTotalPublcMethods());
			System.out.println("The Total Members in this class are "
					+ statistics.getTotalMembers());
			System.out
					.println("The total amount of paramaters for each method are showed in the following list:\n");

			// for each parameter add it to the print ln
			for (int i = 0; i < statistics.getParamaterAmounts().length; i++) {
				System.out.print(statistics.getParamaterAmounts()[i] + " ");
			}
			System.out.println("\n");
		} else {
			System.out.println("Class is not in hash table");
		}

	}

	/**
	 * Get all of the class statistics from a certain class.
	 * 
	 * @param className
	 *            The class you would like to get details for.
	 * @return The statistics of the class name passed in.
	 */
	public ClassStatistics getClassStatistics(String className) {
		ClassStatistics statistics = null;
		Class<?> forEnumeration = null;

		try {
			forEnumeration = Class.forName(className);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		// get required values and set up class
		statistics = new ClassStatistics();
		Field[] variables = forEnumeration.getDeclaredFields();
		Method[] methods = forEnumeration.getDeclaredMethods();

		statistics.setTotalMembers(variables.length + methods.length);
		statistics.setTotalMethods(methods.length);
		statistics.setTotalPublcMethods(publicMethodAmount(methods));
		statistics.setParamaterAmounts(methodParamaterAmounts(methods));

		dataStructure.put(className, statistics);

		return statistics;
	}

	/**
	 * Method to recursively get the class statistics for named class and all
	 * classes it creates and its super class.
	 * 
	 * @param className
	 *            Name of base class to get statistics of.
	 */
	public void recursiveClassStatistics(String className) {
		ClassStatistics baseClass = getClassStatistics(className);

		String[] referredClasses = baseClass.getReferredClass(className);
		printClassStatistics(className);

		if (referredClasses != null) {
			for (String referredClass : referredClasses) {
				getClassStatistics(referredClass);
				printClassStatistics(referredClass);
				recursiveClassStatistics(referredClass);
			}
		}
	}

	/**
	 * Method to read in a file and get statistics on multiple classes, but also
	 * on the classes they create and use.
	 * 
	 * @param fileName
	 *            The name of the file containing the classes to search.
	 */
	public void recursiveMultipleClassStatistics(String fileName) {

		String readLine;
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(fileName));

			// while there is still more to read, get statics for class and put
			// it into the hashtable. The print out the statistics.
			while ((readLine = reader.readLine()) != null) {
				recursiveClassStatistics(readLine);
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}

	}

	/**
	 * Method to get the amount of parameters for each method of a class.
	 * 
	 * @param methods
	 *            Array of methods to get parameter listings for.
	 * @return Array of integers containing the parameter amounts for each
	 *         method.
	 */
	private int[] methodParamaterAmounts(Method[] methods) {
		int[] paramaterAmounts = new int[methods.length];

		// for each method put the amount of parameters into the
		// paramaterAmounts array
		for (int i = 0; i < methods.length; i++) {
			paramaterAmounts[i] = methods[i].getParameterTypes().length;
		}

		return paramaterAmounts;
	}

	/**
	 * Method to get the amount of Public Methods from a list of methods.
	 * 
	 * @param Methods
	 *            List of methods to search through.
	 * @return Integer value containing amount of public methods.
	 */
	private int publicMethodAmount(Method[] methods) {
		int methodAmount = 0;

		// for each method, check if its modifier is public and increase method
		// amount if necessary
		for (Method method : methods) {
			if (Modifier.isPublic(method.getModifiers())) {
				methodAmount++;
			}
		}

		return methodAmount;
	}
}