package uk.ac.aber.bea6.Reflection;
/**
 * @author Ben Ashwell(bea6)
 * 
 */
public class ClassStatistics {
	/** Amount of public methods in class */
	private int totalPublcMethods;
	/** Amount of parameters passed to each method */
	private int[] paramaterAmounts;
	/** Amount of methods in class */
	private int totalMethods;
	/** Amount of members in class */
	private int totalMembers;

	/**
	 * Constructor for class, to set the name of the class to hold details
	 * about.
	 * 
	 * @param className
	 *            Name of the class to hold statistics about.
	 */
	public ClassStatistics() {
	}

	/**
	 * @return the totalPublcMethods
	 */
	public int getTotalPublcMethods() {
		return totalPublcMethods;
	}

	/**
	 * @param totalPublcMethods
	 *            the totalPublcMethods to set
	 */
	public void setTotalPublcMethods(int totalPublcMethods) {
		this.totalPublcMethods = totalPublcMethods;
	}

	/**
	 * @return the paramaterAmounts
	 */
	public int[] getParamaterAmounts() {
		return paramaterAmounts;
	}

	/**
	 * @param paramaterAmounts
	 *            the paramaterAmounts to set
	 */
	public void setParamaterAmounts(int[] paramaterAmounts) {
		this.paramaterAmounts = paramaterAmounts;
	}

	/**
	 * @return the totalMethods
	 */
	public int getTotalMethods() {
		return totalMethods;
	}

	/**
	 * @param totalMethods
	 *            the totalMethods to set
	 */
	public void setTotalMethods(int totalMethods) {
		this.totalMethods = totalMethods;
	}

	/**
	 * @return the totalMembers
	 */
	public int getTotalMembers() {
		return totalMembers;
	}

	/**
	 * @param totalMembers
	 *            the totalMembers to set
	 */
	public void setTotalMembers(int totalMembers) {
		this.totalMembers = totalMembers;
	}

	/**
	 * Method to get the referred classes of a specified class name.
	 * 
	 * @param className
	 *            name of the class to find referred classes of.
	 * @return An array of class names that the parameter name refers to.
	 */
	public String[] getReferredClass(String className) {
		String[] referredClasses = null;

		Class<?> forEnumeration = null;

		try {
			forEnumeration = Class.forName(className);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		// Adapter? this seems like more effort but I have wrote a method as
		// shown by the signature in assignment
		Class<?>[] referredClassesArray = getReferredClass(forEnumeration);
		if(referredClassesArray.length != 0 && referredClassesArray[0] != null)
		{
			referredClasses = new String[referredClassesArray.length];
		
			for (int i = 0; i < referredClassesArray.length; i++) {
				referredClasses[i] = referredClassesArray[i].getName();
			}
		}
		return referredClasses;
	}

	/**
	 * Method to get all the referred classes of a class passed in.
	 * 
	 * @param forEnumeration
	 *            Class to find referred classes.
	 * @return an array of classes that is referred to by parameter class.
	 */
	private Class<?>[] getReferredClass(Class<?> forEnumeration) {

		Class<?>[] referredClasses = null;
		Class<?>[] temp = forEnumeration.getDeclaredClasses();
		referredClasses = new Class<?>[temp.length + 1];

		// make the first element the super class
		referredClasses[0] = forEnumeration.getSuperclass();

		// copy elements of temp into referred class after the first element
		if (temp.length != 0) {
			for (int i = 1; i <= temp.length; i++) {
				referredClasses[i] = temp[(i - 1)];
			}
		}

		return referredClasses;
	}

}
